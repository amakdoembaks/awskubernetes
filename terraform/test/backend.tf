terraform {
  backend "s3" {
    bucket  = "clientsafe-terraform-state"
    key     = "terraform.kubernetes.test.tfstate"
    region  = "eu-west-1"
    encrypt = true
    profile = "test"
  }

  required_version = "~> 0.11"
}

provider "aws" {
  region  = "${var.region}"
  version = "1.44"
  profile = "test"
}

provider "aws" {
  alias   = "shared-account"
  region  = "${var.region}"
  version = "1.44"
  profile = "test"
}
