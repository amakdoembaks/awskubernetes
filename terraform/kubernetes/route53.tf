data "aws_route53_zone" "bastion" {
  name     = "${var.dns_zone_bastion}."
  provider = "aws.shared-account"
}

resource "aws_route53_record" "bastion" {
  zone_id = "${data.aws_route53_zone.bastion.zone_id}"
  name    = "bastion.ku.${local.environment_short}.${var.dns_zone_bastion}"
  type    = "A"
  ttl     = 30
  records = ["${aws_eip.bastion.public_ip}"]
  provider = "aws.shared-account"
}
