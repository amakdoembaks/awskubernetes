resource "aws_iam_group" "developers_group" {
  name = "${local.env_name}-developers"
}

output "iam_group_name" {
  value = "${aws_iam_group.developers_group.name}"
}

resource "aws_iam_group_policy" "developers_group_policy" {
  name  = "${local.env_name}-developers-policy"
  group = "${aws_iam_group.developers_group.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "*",
      "Resource": "*"
    }, {
      "Effect": "Deny",
      "Action": ["ses:SendEmail", "ses:SendRawEmail"],
      "Resource": "*",
      "Condition": {
        "ForAnyValue:StringNotLike": {
          "ses:Recipients": [
              "*@ksyos.org",
              "*@ksyos.nl",
              "*@bi4group.com"
          ]
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_account_password_policy" "strict" {
  minimum_password_length        = 12
  allow_users_to_change_password = true
}

resource "aws_iam_user" "deploy" {
  name          = "${local.env_name}-deploy"
  force_destroy = true
}

