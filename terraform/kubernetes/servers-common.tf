// IAM profile for the servers role
resource "aws_iam_instance_profile" "servers" {
  name = "${local.env_name}-servers"
  role = "${aws_iam_role.servers.name}"
}

// IAM role that the servers will assume
resource "aws_iam_role" "servers" {
  name = "${local.env_name}-servers"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "download_deploy" {
  name = "${local.env_name}-download-deploy"
  role = "${aws_iam_role.servers.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": ["s3:ListBucket"],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.provision_bucket.bucket}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": ["s3:GetObject"],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.provision_bucket.bucket}/*"
            ]
        }
    ]
}
EOF
}

resource "aws_s3_bucket_object" "ansible-vars" {
  key                    = "ansible-vars.yml"
  bucket                 = "${aws_s3_bucket.provision_bucket.bucket}"
  content                = "${file("${path.root}/ansible-vars.yml")}"
  server_side_encryption = "aws:kms"

  tags {
    Name        = "${var.environment} socket permissions"
    Environment = "${var.environment}"
  }

  depends_on = ["aws_s3_bucket_policy.provision_bucket"]
}

resource "aws_s3_bucket_object" "users-playbook" {
  key                    = "users.yml"
  bucket                 = "${aws_s3_bucket.provision_bucket.bucket}"
  content                = "${file("${path.root}/../kubernetes/files/users.yml")}"
  server_side_encryption = "aws:kms"

  tags {
    Name        = "${var.environment} socket permissions"
    Environment = "${var.environment}"
  }

  depends_on = ["aws_s3_bucket_policy.provision_bucket"]
}

resource "aws_s3_bucket_object" "master" {
  key                    = "master.yml"
  bucket                 = "${aws_s3_bucket.provision_bucket.bucket}"
  content                = "${file("${path.root}/../kubernetes/files/master.yml")}"
  server_side_encryption = "aws:kms"

  tags {
    Name        = "${var.environment} socket permissions"
    Environment = "${var.environment}"
  }

  depends_on = ["aws_s3_bucket_policy.provision_bucket"]
}

resource "aws_s3_bucket_object" "init_master" {
  key                    = "init_master.sh"
  bucket                 = "${aws_s3_bucket.provision_bucket.bucket}"
  content                = "${file("${path.root}/../kubernetes/files/init_master.sh")}"
  server_side_encryption = "aws:kms"

  tags {
    Name        = "${var.environment} socket permissions"
    Environment = "${var.environment}"
  }

  depends_on = ["aws_s3_bucket_policy.provision_bucket"]
}

resource "aws_s3_bucket" "provision_bucket" {
  bucket = "${local.env_name}-provisioning"
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }

  versioning {
    enabled = true
  }

  tags {
    Name        = "${local.name_prefix} Provisioning"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

resource "aws_s3_bucket_policy" "provision_bucket" {
  bucket = "${aws_s3_bucket.provision_bucket.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:PutObject",
            "Resource": "${aws_s3_bucket.provision_bucket.arn}/*",
            "Condition": {
                "StringNotEquals": {
                    "s3:x-amz-server-side-encryption": "aws:kms"
                },
                "Null": {
                    "s3:x-amz-server-side-encryption": "false"
                }
            }
        }, {
            "Effect": "Deny",
            "Principal": "*",
            "Action": "s3:PutObject",
            "Resource": "${aws_s3_bucket.provision_bucket.arn}/*",
            "Condition": {
                "StringNotEquals": {
                    "s3:x-amz-acl": "private"
                },
                "Null": {
                    "s3:x-amz-acl": "false"
                }
            }
        }
    ]
}
EOF
}
