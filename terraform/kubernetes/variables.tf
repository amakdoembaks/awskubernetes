variable "region" {
  default = "eu-west-1"
}

variable "environment" {}

variable "availability_zones" {
  default = ["eu-west-1a", "eu-west-1b"]
}

variable "rds_multi_az" {
  default = false
}

variable "application" {
  default = "Kubernetes"
}

variable "application_tag" {
  default = "KU"
}

variable "application_short" {
  default = "ku"
}

variable "use_validated_ami" {
  default = true
}

variable "worker_instance_type" {
  default = "t2.large"
}

variable "log_instance_type" {
  default = "t2.small.elasticsearch"
}

variable "log_instance_count_per_az" {
  default = 1
}

variable "dns_zone_bastion" {
  default = "ksyos-tech.org"
}

locals {
  environment_short = "${lower(var.environment)}"
  env_name          = "${var.application_short}-${local.environment_short}"
  name_prefix       = "${var.application_tag} ${var.environment}"
}
