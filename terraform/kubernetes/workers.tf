resource "aws_instance" "kubernetes_server" {
  ami                         = "ami-08d658f84a6d84a80"
  instance_type               = "t2.medium"
  availability_zone           = "${element(var.availability_zones, 0)}"
  monitoring                  = true
  associate_public_ip_address = true
  disable_api_termination     = true
  key_name                    = "ubuntu_server"
  iam_instance_profile        = "${aws_iam_instance_profile.servers.id}"

  vpc_security_group_ids = [
    "${aws_security_group.allow_incoming_http_and_https.id}",
    "${aws_security_group.allow_outgoing_http_and_https.id}",
    "${aws_security_group.allow_incoming_ssh_from_bastion.id}",
  ]

  subnet_id = "${element(aws_subnet.public.*.id, 0)}"

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "100"
    delete_on_termination = false
  }

  tags {
    Name        = "${local.name_prefix} Kubernetes Master server"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }

  volume_tags {
    Name        = "${local.name_prefix} Kubernetes Master server volume"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }

  user_data = <<EOF
#!/bin/bash

sudo apt-get --assume-yes install python
sudo apt-add-repository ppa:ansible/ansible -y
sudo apt-get update

sudo apt-get --assume-yes upgrade
sudo apt-get --assume-yes install ansible
export DEBIAN_FRONTEND=noninteractive
sudo apt-get --assume-yes install python-pip unzip zip
sudo pip install awscli

sudo apt-get update
sudo apt-get --assume-yes install apt-transport-https ca-certificates curl software-properties-common
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
sudo add-apt-repository "deb https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"
sudo apt-get update

sudo apt-get --assume-yes install docker-ce=$(apt-cache madison docker-ce | grep 18.03 | head -1 | awk '{print $3}')

sudo apt-get --assume-yes install apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

echo deb http://apt.kubernetes.io/ kubernetes-xenial main > /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update

sudo apt-get --assume-yes install kubelet kubeadm kubectl
sysctl net.bridge.bridge-nf-call-iptables=1
systemctl daemon-reload

aws s3 cp --recursive s3://${aws_s3_bucket.provision_bucket.id} ~
echo -e "[workers]\nlocalhost ansible_connection=local" > ~/local-inventory.ini
ansible-playbook -i ~/local-inventory.ini -e 'users_file=~/ansible-vars.yml' ~/users.yml

sudo sleep 3
sudo reboot
EOF

}

resource "aws_eip" "kubernetes_server" {
  vpc = true
}

resource "aws_eip_association" "kubernetes_server_eip" {
  instance_id   = "${aws_instance.kubernetes_server.id}"
  allocation_id = "${aws_eip.kubernetes_server.id}"
}