// The VPC everything will run in.
resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"

  tags {
    Name        = "${local.name_prefix} vpc"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Internet gateway that connects the public subnets with the internet.
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name        = "${local.name_prefix} Internet gateway"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Routing table for the public subnets.
resource "aws_route_table" "public_routing" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  tags {
    Name        = "${local.name_prefix} Public subnets routing table"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Public subnets.
resource "aws_subnet" "public" {
  count             = "${length(var.availability_zones)}"
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "10.0.${count.index * 16}.0/20"
  availability_zone = "${element(var.availability_zones, count.index)}"

  tags {
    Name        = "${local.name_prefix} Public subnet ${upper(substr(element(var.availability_zones, count.index), -1, -1))}"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Associate public subnets with the routing table.
resource "aws_route_table_association" "public" {
  count          = "${length(var.availability_zones)}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public_routing.id}"
}

// Elastic IPs for the NAT gateways that allow outgoing internet access from the private subnets.
resource "aws_eip" "nat" {
  vpc = true
}

// The NAT gateways that allow outgoing internet access from the private subnets.
resource "aws_nat_gateway" "nat" {
  allocation_id = "${element(aws_eip.nat.*.id, count.index)}"
  subnet_id     = "${element(aws_subnet.public.*.id, count.index)}"
}

// Routing tables for the private subnets.
resource "aws_route_table" "private_routing" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${element(aws_nat_gateway.nat.*.id, count.index)}"
  }

  tags {
    Name        = "${local.name_prefix} Private subnets routing table using NAT ${upper(substr(element(var.availability_zones, count.index), -1, -1))}"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Private subnets.
resource "aws_subnet" "private" {
  count             = "${length(var.availability_zones)}"
  vpc_id            = "${aws_vpc.vpc.id}"
  cidr_block        = "10.0.${128 + count.index * 16}.0/20"
  availability_zone = "${element(var.availability_zones, count.index)}"

  tags {
    Name        = "${local.name_prefix} Private subnet ${upper(substr(element(var.availability_zones, count.index), -1, -1))}"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Associate private subnets with the routing table.
resource "aws_route_table_association" "private" {
  count          = "${length(var.availability_zones)}"
  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.private_routing.*.id, count.index)}"
}
