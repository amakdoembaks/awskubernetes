// Empty ACL for public subnets.
resource "aws_network_acl" "public_acl" {
  vpc_id     = "${aws_vpc.vpc.id}"
  subnet_ids = ["${aws_subnet.public.*.id}"]

  tags {
    Name        = "${local.name_prefix} Public subnet ACL"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Empty ACL for private subnets.
resource "aws_network_acl" "private_acl" {
  vpc_id     = "${aws_vpc.vpc.id}"
  subnet_ids = ["${aws_subnet.private.*.id}"]

  tags {
    Name        = "${local.name_prefix} Private subnet ACL"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

/// public incoming

// Allow incoming SSH connections into the public subnets.
resource "aws_network_acl_rule" "public_allow_incoming_ssh" {
  network_acl_id = "${aws_network_acl.public_acl.id}"
  rule_number    = 100
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 22
  to_port        = 22
}

// Allow incoming HTTP connections into the public subnets.
resource "aws_network_acl_rule" "public_allow_incoming_http" {
  network_acl_id = "${aws_network_acl.public_acl.id}"
  rule_number    = 110
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}

// Allow incoming HTTPS connections into the public subnets.
resource "aws_network_acl_rule" "public_allow_incoming_https" {
  network_acl_id = "${aws_network_acl.public_acl.id}"
  rule_number    = 120
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 443
  to_port        = 443
}

// Allow incoming return traffic on ephemeral ports into the public subnets. 
resource "aws_network_acl_rule" "public_allow_incoming_return_trafic" {
  network_acl_id = "${aws_network_acl.public_acl.id}"
  rule_number    = 1000
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}

/// public outgoing

// Allow outging SSH connections from the public subnets to the private subnets.
resource "aws_network_acl_rule" "public_allow_ssh_to_private" {
  network_acl_id = "${aws_network_acl.public_acl.id}"
  rule_number    = 100
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.128.0/17"
  from_port      = 22
  to_port        = 22
}

// Allow outgoing HTTP connections from the public subnets.
resource "aws_network_acl_rule" "public_allow_outgoing_http" {
  network_acl_id = "${aws_network_acl.public_acl.id}"
  rule_number    = 110
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}

// Allow outging HTTPS connections from the public subnets.
resource "aws_network_acl_rule" "public_allow_outgoing_https" {
  network_acl_id = "${aws_network_acl.public_acl.id}"
  rule_number    = 120
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 443
  to_port        = 443
}

// Allow outgoing traffic to ephemeral ports from the NAT gateways in the public subnets.
resource "aws_network_acl_rule" "public_allow_outgoing_return_trafic" {
  network_acl_id = "${aws_network_acl.public_acl.id}"
  rule_number    = 1000
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}

/// private incoming

// Allow incoming SSH connections from the public subnets into the private subnets.
resource "aws_network_acl_rule" "private_allow_incoming_ssh" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 100
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.0.0/17"
  from_port      = 22
  to_port        = 22
}

// Allow incoming HTTPS connections from other subnets into the private subnets.
resource "aws_network_acl_rule" "private_allow_incoming_https" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 120
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.0.0/16"
  from_port      = 443
  to_port        = 443
}

// Allow incoming postgres connections from the other private subnets.
resource "aws_network_acl_rule" "private_allow_incoming_postgres" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 130
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.128.0/17"
  from_port      = 5432
  to_port        = 5432
}

// Allow incoming SQL server connections from the other private subnets.
resource "aws_network_acl_rule" "private_allow_incoming_sql_server" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 140
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.128.0/17"
  from_port      = 1433
  to_port        = 1433
}

// Allow incoming return traffic from the NAT gateway to the private subnets on ephemeral ports.
resource "aws_network_acl_rule" "private_allow_incoming_return_traffic" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 1000
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 32768
  to_port        = 61000
}

// Allow incoming Postgres connections from public subnet.
resource "aws_network_acl_rule" "private_allow_incoming_postgres_from_server" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 2000
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.0.0/16"
  from_port      = 5432
  to_port        = 5432
}

/// private outgoing

// Allow outgoing HTTP connections from the private subnets.
resource "aws_network_acl_rule" "private_allow_outgoing_http" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 100
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 80
  to_port        = 80
}

// Allow ouging HTTPS connections from the private subnets.
resource "aws_network_acl_rule" "private_allow_outgoing_https" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 110
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 443
  to_port        = 443
}

// Allow outgoing postgres connections to the other private subnets.
resource "aws_network_acl_rule" "private_allow_outgoing_postgres" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 120
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.128.0/17"
  from_port      = 5432
  to_port        = 5432
}

// Allow outgoing SQL server connections to the other private subnets.
resource "aws_network_acl_rule" "private_allow_outgoing_sql_server" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 130
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.128.0/17"
  from_port      = 1433
  to_port        = 1433
}

// Allow outging traffic on ephemeral ports from the private subnets to the other subnets (return traffic to postgres).
resource "aws_network_acl_rule" "private_allow_outgoing_trafic" {
  network_acl_id = "${aws_network_acl.private_acl.id}"
  rule_number    = 1000
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "10.0.0.0/16"
  from_port      = 1024
  to_port        = 65535
}
