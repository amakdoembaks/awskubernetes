data "aws_ami" "centos7_ami" {
  most_recent = true
  owners      = ["self"]

  filter {
    name   = "name"
    values = ["CentOS 7 Encrypted*"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }

  filter {
    name   = "tag:Validated"
    values = ["Yes"]
  }
}

// Bastion server.
resource "aws_instance" "bastion" {
  ami               = "${data.aws_ami.centos7_ami.id}"
  instance_type     = "t2.micro"
  availability_zone = "${element(var.availability_zones, 0)}"
  monitoring        = true

  iam_instance_profile = "${aws_iam_instance_profile.bastion-server.id}"
  
  vpc_security_group_ids = [
    "${aws_security_group.bastion_ssh.id}",
    "${aws_security_group.allow_outgoing_http_and_https.id}",
  ]

  subnet_id = "${element(aws_subnet.public.*.id, 0)}"

  tags {
    Name        = "${local.name_prefix} Bastion server"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }

  volume_tags {
    Name        = "${local.name_prefix} Bastion server volume"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }

  user_data = <<EOF
#!/bin/bash
echo ${base64encode(file("${path.root}/../kubernetes/files/users.yml"))} | base64 --decode > ~/users.yml
echo ${base64encode(file("${path.root}/../kubernetes/ansible-vars.yml"))} | base64 --decode > ~/ansible-vars.yml
echo -e "[bastion]\nlocalhost ansible_connection=local\n" > ~/local-inventory.ini
ansible-playbook -i ~/local-inventory.ini -e 'users_file=~/ansible-vars.yml' -e 'is_bastion=True' ~/users.yml
EOF
}

resource "aws_eip" "bastion" {
  vpc = true
}

resource "aws_eip_association" "bastion_eip" {
  instance_id   = "${aws_instance.bastion.id}"
  allocation_id = "${aws_eip.bastion.id}"
}

resource "aws_iam_instance_profile" "bastion-server" {
  name = "${local.env_name}-bastion-server"
  role = "${aws_iam_role.bastion-server.name}"
}

resource "aws_iam_role" "bastion-server" {
  name = "${local.env_name}-bastion-server"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "bastion-server" {
  name = "${local.env_name}-bastion-server"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:DescribeInstances"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "bastion-server-attach" {
  role       = "${aws_iam_role.bastion-server.name}"
  policy_arn = "${aws_iam_policy.bastion-server.arn}"
}