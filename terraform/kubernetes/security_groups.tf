// Security group that allows incoming SSH trafic from the internet. Should be applied only to the bastion servers.
resource "aws_security_group" "bastion_ssh" {
  name        = "${local.env_name}-bastion-ssh"
  description = "Allow incoming SSH connection from the internet and outgoing to the vpc"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
  }

  tags {
    Name        = "${local.name_prefix} allow incoming SSH"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

resource "aws_security_group_rule" "allow_outgoing_ssh_internal" {
  type                     = "egress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.bastion_ssh.id}"
  source_security_group_id = "${aws_security_group.allow_incoming_ssh_from_bastion.id}"
}

// Group that allows incoming SSH connections from the bastion server. Should be applied to every instance in the private subnet so they can be connected to.
resource "aws_security_group" "allow_incoming_ssh_from_bastion" {
  name        = "${local.env_name}-allow-incoming-ssh-from-bastion"
  description = "Allow incoming SSH connections from the bastion servers"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    security_groups = ["${aws_security_group.bastion_ssh.id}"]
    protocol        = "tcp"
    from_port       = 22
    to_port         = 22
  }

  tags {
    Name        = "${local.name_prefix} allow incoming SSH from bastion"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Group that allows outging HTTP and HTTPS traffic. Should be applied to every instance so they can at least update through yum.
resource "aws_security_group" "allow_outgoing_http_and_https" {
  name        = "${local.env_name}-allow-outgoing-http-and-https"
  description = "Allow outgoing HTTP and HTTPS connections"
  vpc_id      = "${aws_vpc.vpc.id}"

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
  }

  tags {
    Name        = "${local.name_prefix} allow outgoing HTTP and HTTPS connections"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Group that allows incomimg https connections from other servers, for the logger elasticsearch
resource "aws_security_group" "allow_incoming_https_internal" {
  name        = "${local.env_name}-allow-incoming-https-internal"
  description = "Allow incoming internal https connections"
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    cidr_blocks = ["10.0.0.0/16"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
  }

  tags {
    Name        = "${local.name_prefix} allow incoming internal https connections"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Group that allows outging HTTP and HTTPS traffic.
resource "aws_security_group" "allow_outgoing_http_https" {
  name        = "${local.env_name}-allow-outgoing-http-https-kubernetes"
  description = "Allow outgoing HTTP HTTPS connections."
  vpc_id      = "${aws_vpc.vpc.id}"

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
  }

  tags {
    Name        = "${local.name_prefix} allow outgoing http https connection"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}

// Group that allows incoming HTTP and HTTPS from the internet.
resource "aws_security_group" "allow_incoming_http_and_https" {
  name        = "${local.env_name}-allow-incoming-http-and-https-kubernetes"
  description = "Allow incoming http and HTTPS connections."
  vpc_id      = "${aws_vpc.vpc.id}"

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
  }

   ingress {
    cidr_blocks = ["0.0.0.0/0"]
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
  }

  tags {
    Name        = "${local.name_prefix} incoming http and https from the internet"
    Application = "${var.application}"
    Environment = "${var.environment}"
  }
}
